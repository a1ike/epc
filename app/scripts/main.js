function hideAllError(str) { return true; }
window.onerror = hideAllError;

$(document).ready(function () {

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  $('.e-qa-card').on('click', function (e) {
    e.preventDefault();

    $(this).children().children().next().slideToggle('fast');
    $(this).children().next().toggleClass('e-qa-card__close_active');
  });

  new Swiper('.e-cases__cards', {
    navigation: {
      nextEl: '.e-cases .swiper-button-next',
      prevEl: '.e-cases .swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    allowTouchMove: false
  });

  new Swiper('.e-papers__cards', {
    navigation: {
      nextEl: '.e-papers .swiper-button-next',
      prevEl: '.e-papers .swiper-button-prev',
    },
    slidesPerView: 1,
    spaceBetween: 0,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('.e-gallery', {
    navigation: {
      nextEl: '.e-gallery .swiper-button-next',
      prevEl: '.e-gallery .swiper-button-prev',
    },
    spaceBetween: 130,
    centerMode: true,
    loop: true,
  });

  $('.e-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.e-header__row').slideToggle('fast');
    $('.e-header__wrap').slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.e-modal').toggle();
  });

  $('.e-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.e-modal').toggle();
  });

  var galleryThumbs = new Swiper('.e-team__preview', {
    direction: 'vertical',
    allowTouchMove: false,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  new Swiper('.e-team__view', {
    on: {
      slideNextTransitionEnd: function () {
        $('.e-team__view-card-svg').css({
          '-webkit-transform': 'rotate(' + 0 + 'deg)',
          '-moz-transform': 'rotate(' + 0 + 'deg)',
          '-ms-transform': 'rotate(' + 0 + 'deg)',
          'transform': 'rotate(' + 0 + 'deg)'
        });
        $('.swiper-slide-active .e-team__view-card-svg').css({
          '-webkit-transform': 'rotate(' + 45 + 'deg)',
          '-moz-transform': 'rotate(' + 45 + 'deg)',
          '-ms-transform': 'rotate(' + 45 + 'deg)',
          'transform': 'rotate(' + 45 + 'deg)'
        });
      },
      slidePrevTransitionEnd: function () {
        $('.e-team__view-card-svg').css({
          '-webkit-transform': 'rotate(' + 0 + 'deg)',
          '-moz-transform': 'rotate(' + 0 + 'deg)',
          '-ms-transform': 'rotate(' + 0 + 'deg)',
          'transform': 'rotate(' + 0 + 'deg)'
        });
        $('.swiper-slide-active .e-team__view-card-svg').css({
          '-webkit-transform': 'rotate(' + -45 + 'deg)',
          '-moz-transform': 'rotate(' + -45 + 'deg)',
          '-ms-transform': 'rotate(' + -45 + 'deg)',
          'transform': 'rotate(' + -45 + 'deg)'
        });
      }
    },
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    navigation: {
      nextEl: '.e-team .swiper-button-next',
      prevEl: '.e-team .swiper-button-prev',
    },
    thumbs: {
      swiper: galleryThumbs
    }
  });

  var galleryThumbs2 = new Swiper('.e-cases__small', {
    spaceBetween: 18,
    slidesPerView: 'auto',
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  new Swiper('.e-cases__big', {
    spaceBetween: 10,
    thumbs: {
      swiper: galleryThumbs2
    }
  });
});